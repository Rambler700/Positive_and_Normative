# -*- coding: utf-8 -*-
"""
Created on Thu Mar  8 16:11:27 2018

@author: enovi
"""

import nltk

#positive and normative economics
#count positive and normative words

def main():
    choice = True
    article_text = ''
    c = 'y'
    print('Welcome to Positive/Normative Analyzer!')
    while choice == True:
         c = input('Analyze an article y/n? ')
         if c == 'y':
             article_text = get_text()
             analyze_text(article_text)
             choice = True
         elif c == 'n':
             choice = False
             
def get_text():
    article_text = ''
    article_text = str(input('Enter the message here: '))
    return article_text

def analyze_text(article_text):

    positive_list   = ['account', 'adjust', 'assign', 'borrow', 'budget', 'contract', 'cost',
                       'count', 'credit', 'debit', 'debt', 'decrease', 'earning', 'economy',
                       'expand', 'expansion', 'expense', 'fall', 'fee', 'finance', 'gross',
                       'increase', 'incur', 'lend', 'lower', 'margin', 'money', 'operate',
                       'pay', 'plan', 'profit', 'project', 'raise', 'reconcile', 'revenue',
                       'rise', 'salary', 'scale', 'spend', 'tax', 'wage']
    
    normative_list  = ['accurate', 'aim', 'align', 'appeal', 'benefit', 'correct', 'cruel',
                       'duty', 'earn', 'equal', 'equity', 'ethic', 'external', 'fair', 'free',
                       'goal', 'guard', 'harsh', 'ideal', 'just', 'lead', 'moral', 'must',
                       'oblige', 'pain', 'penal', 'progress', 'protect', 'punish', 'quality',
                       'reward', 'right', 'safe', 'should', 'social', 'stance', 'stand',
                       'subsidy', 'sustain', 'target', 'true', 'welfare', 'will']
    
    positive_count  = 0
    normative_count = 0
    article_text   = article_text.lower().split()
    
    for word in article_text:
        word = nltk.PorterStemmer().stem(word)
        
    for word in positive_list:
        word = nltk.PorterStemmer().stem(word)
        
    for word in normative_list:
        word = nltk.PorterStemmer().stem(word)
    
    for word in article_text:
        if word in positive_list:
            positive_count += 1
        elif word in normative_list:
            normative_count += 1
            
    if positive_count > 1:        
        print('There were {} positive words.'.format(positive_count))
    elif positive_count == 1:
        print('There was one positive word.')
    else:
        print('There were no positive words.')
    if normative_count > 1:
        print('There were {} normative words.'.format(normative_count))        
    elif normative_count == 1:
        print('There was one normative word.')
    else:
        print('There were no normative words.')
main()